package challenge.game;

public enum PieceType {
	Rook, Bishop, Knight, Queen, King
}
