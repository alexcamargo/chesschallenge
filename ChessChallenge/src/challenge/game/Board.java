package challenge.game;

import java.util.HashMap;
import java.util.Map;

import challenge.game.behaviors.BishopBehavior;
import challenge.game.behaviors.IPieceBehavior;
import challenge.game.behaviors.KingBehavior;
import challenge.game.behaviors.KnightBehavior;
import challenge.game.behaviors.QueenBehavior;
import challenge.game.behaviors.RookBehavior;

public class Board {

	private int width;
	private int height;
	private int[][] boardSlots;
	
	private final Increase increase = new Increase();
	private final Decrease decrease = new Decrease();
	
	private final Map<PieceType, IPieceBehavior> behaviorMap;
	
	public Board(int height, int width) {
		this.width = width;
		this.height = height;
		
		boardSlots = new int[this.height][];
		for(int i =0; i< this.height; i++){
			boardSlots[i] = new int[this.width]; 
		}
		
		behaviorMap = new HashMap<PieceType, IPieceBehavior>();
		behaviorMap.put(PieceType.Bishop, new BishopBehavior(this));
		behaviorMap.put(PieceType.King, new KingBehavior(this));
		behaviorMap.put(PieceType.Knight, new KnightBehavior(this));
		behaviorMap.put(PieceType.Queen, new QueenBehavior(this));
		behaviorMap.put(PieceType.Rook, new RookBehavior(this));		
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}
	
	public Increase getIncrease() {
		return increase;
	}
	
	public Decrease getDecrease() {
		return decrease;
	}
	
	public int getSlotValue(int x, int y) {
		return boardSlots[x][y];
	}

	private IPieceBehavior getBehavior(PieceType pieceType) {
		if(!behaviorMap.containsKey(pieceType))
			throw new RuntimeException("Can't find behavior for the given piece type: "+ pieceType);
		return behaviorMap.get(pieceType);
	}
	
	public void putPiece(Piece piece) {
		getBehavior(piece.getPieceType()).increaseThreat(piece.getX(),piece.getY());
		boardSlots[piece.getX()][piece.getY()] = -1;
	}
	
	public void removePiece(Piece piece) {
		getBehavior(piece.getPieceType()).decreaseThreat(piece.getX(),piece.getY());
		boardSlots[piece.getX()][piece.getY()] = 0;
	}
		
	public boolean canPutPieceInSlot(PieceType pieceType, int x, int y) {
		if(boardSlots[x][y] != 0)
			return false;
		
		return getBehavior(pieceType).verify(x, y);
	}

	// Commands to increase or decrease threat of a slot
	public interface Command{
		void execute(int x, int y);
	}
	public class Increase implements Command{
		@Override
		public void execute(int x, int y) {
			boardSlots[x][y]++;			
		}
	}
	
	public class Decrease implements Command{
		@Override
		public void execute(int x, int y) {
			boardSlots[x][y]--;			
		}
	}
	
}
