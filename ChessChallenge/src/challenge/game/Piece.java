package challenge.game;

public abstract class  Piece {
	private int x;
	private int y;
	private PieceType pieceType;
	
	public Piece(PieceType pieceType){
		this(0,0,pieceType);
	}
	
	public Piece(int x, int y, PieceType pieceType) {
		this.x = x;
		this.y = y;
		this.pieceType = pieceType;
	}
	
	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public PieceType getPieceType() {
		return pieceType;
	}
	
	public char getType(){
		switch(pieceType){
			case Bishop:
				return 'B';
			case King:
				return 'K';
			case Queen:
				return 'Q';
			case Knight:
				return 'N';
			case Rook:
				return 'R';
		}
		return 'X';
	}
	
	@Override
	public int hashCode() {
		return x*100000+y*1000+getType();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null || !(obj instanceof Piece))
			return false;
		Piece piece = (Piece)obj;
		return this.pieceType == piece.pieceType 
				&& this.x == piece.x && this.y == piece.y;
	}
	
	@Override
	public String toString() {
		return String.format("[%1$s, x=%2$s,y=%3$s]", this.getType(), this.x,this.y);
	}

}
