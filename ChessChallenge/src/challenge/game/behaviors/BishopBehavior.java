package challenge.game.behaviors;

import challenge.game.Board;

public class BishopBehavior implements IPieceBehavior {
	
	private DiagonalBehavior diagonalBehavior;
	
	public BishopBehavior(Board board) {
		diagonalBehavior = new DiagonalBehavior(board);
	}

	@Override
	public boolean verify(int x, int y) {
		return diagonalBehavior.verify(x,y);
	}

	@Override
	public void increaseThreat(int x, int y) {
		diagonalBehavior.increaseThreat(x, y);
	}

	@Override
	public void decreaseThreat(int x, int y) {
		diagonalBehavior.decreaseThreat(x, y);
	}

}
