package challenge.game.behaviors;

import challenge.game.Board;
import challenge.game.Board.Command;

public class DiagonalBehavior {
	
	private Board board;

	public DiagonalBehavior(Board board){
		this.board = board;
	}
	
	public boolean verify(int x, int y, int maxDistance) {
		boolean upperRight = true,upperLeft= true,bottomRight= true,bottomLeft= true; 
		int i = 1;
		while(i<= maxDistance && (upperRight || upperLeft || bottomRight || bottomLeft)){
			if(upperLeft){
				if((x-i < 0) || (y-i < 0))
					upperLeft = false;
				else if(board.getSlotValue(x-i, y-i) == -1)
					return false;		
			}
			
			if (upperRight){				
				if((x-i < 0) || (y+i > board.getWidth()-1))
					upperRight = false;
				else if(board.getSlotValue(x-i,y+i)== -1)
					return false;
			}
						
			if(bottomLeft){				
				if((x+i > board.getHeight()-1) || (y-i < 0)){
					bottomLeft = false;
				}else if(board.getSlotValue(x+i, y-i)== -1)
					return false;			
			}
			
			if(bottomRight){
				if((x+i >board.getHeight()-1) || (y+i > board.getWidth()-1))
					bottomRight = false;
				else if(board.getSlotValue(x+i, y+i)== -1)
					return false;	
			}			
				
			i++;
		}
		return true;
	}

	public void increaseThreat(int x, int y) {
		increaseThreat(x,y,Integer.MAX_VALUE);
	}
	
	public void increaseThreat(int x, int y, int maxDistance) {
		executeCommandDiagonal(x, y, maxDistance, board.getIncrease());		
	}
	
	public void decreaseThreat(int x, int y) {
		decreaseThreat(x,y,Integer.MAX_VALUE);
	}
	
	public void decreaseThreat(int x, int y, int maxDistance) {
		executeCommandDiagonal(x,y, maxDistance, board.getDecrease());		
	}
	
	private void executeCommandDiagonal(int x, int y, int maxDistance, Command command) {
		boolean upperRight = true,upperLeft= true,bottomRight= true,bottomLeft= true; 
		int i = 1;
		while(i<= maxDistance && (upperRight || upperLeft || bottomRight || bottomLeft)){
			if(upperLeft){
				if((x-i < 0) || (y-i < 0))
					upperLeft = false;
				else command.execute(x-i, y-i);
			}
			
			if (upperRight){				
				if((x-i < 0) || (y+i > board.getWidth()-1))
					upperRight = false;
				else 
					command.execute(x-i, y+i);
			}
						
			if(bottomLeft){				
				if((x+i >board.getHeight()-1) || (y-i < 0))
					bottomLeft = false;
				else 
					command.execute(x+i, y-i);
								
			}
			
			if(bottomRight){
				if((x+i >board.getHeight()-1) || (y+i > board.getWidth()-1))
					bottomRight = false;
				else 
					command.execute(x+i, y+i);	
			}			
				
			i++;
		}
	}

	public boolean verify(int x, int y) {
		return verify(x, y, Integer.MAX_VALUE);
	}

}
