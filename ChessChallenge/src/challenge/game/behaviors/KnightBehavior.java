package challenge.game.behaviors;

import challenge.game.Board;
import challenge.game.Board.Command;

public class KnightBehavior implements IPieceBehavior {

	private Board board;

	public KnightBehavior(Board board) {
		this.board = board;
	}

	public boolean verify(int x, int y) {
		int maxY = board.getWidth();
		int maxX = board.getHeight();
		
		if(x-2 >= 0){
			if(y - 1 >= 0 && board.getSlotValue(x-2, y-1) == -1)
				return false;
			
			if(y +1 < maxY && board.getSlotValue(x-2, y+1) == -1)
				return false;			
		}
		
		if(x + 2 < maxX){
			if (y - 1 >= 0 && board.getSlotValue(x+2, y-1) == -1)
				return false;
			
			if (y +1 < maxY && board.getSlotValue(x+2, y+1) == -1)
				return false;			
		}
		
		if(y - 2 >= 0){
			if(x - 1 >= 0 && board.getSlotValue(x-1, y-2) == -1)
				return false;
			
			if(x +1 < maxX && board.getSlotValue(x+1, y-2) == -1)
				return false;			
		}
		
		if(y + 2 < maxY){
			if(x - 1 >= 0 && board.getSlotValue(x-1, y+2) == -1)
				return false;
			
			if(x +1 < maxX && board.getSlotValue(x+1, y+2) == -1)
				return false;			
		}
		
		return true;		
	}

	public void decreaseThreat(int x, int y) {
		executeCommand(x, y, board.getDecrease());
	}

	public void increaseThreat(int x, int y) {
		executeCommand(x, y, board.getIncrease());
	}
	
	private void executeCommand(int x, int y, Command command) {
		int maxY = board.getWidth();
		int maxX = board.getHeight();
		
		if(x-2 >= 0){
			if(y - 1 >=0)
				command.execute(x-2, y-1);
			
			if(y + 1 < maxY)
				command.execute(x-2, y+1);
		}
		
		if(x + 2 < maxX){
			if (y - 1 >0)
				command.execute(x+2, y-1);
			
			if (y +1 < maxY)
				command.execute(x+2, y+1);	
		}
		
		if(y - 2 >= 0){
			if(x - 1 >= 0)
				command.execute(x-1, y-2);
			
			if(x +1 < maxX)
				command.execute(x+1, y-2);
		}
		
		if(y + 2 < maxY){
			if(x - 1 >= 0)
				command.execute(x-1, y+2);
			
			if(x +1 < maxX)
				command.execute(x+1, y+2);
		}
	}

}
