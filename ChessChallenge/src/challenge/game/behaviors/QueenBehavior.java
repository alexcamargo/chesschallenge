package challenge.game.behaviors;

import challenge.game.Board;

public class QueenBehavior implements IPieceBehavior {
	
	private HorizontalAndVerticalBehavior horizontalAndVerticalBehavior;
	private DiagonalBehavior diagonalBehavior;
	
	public QueenBehavior(Board board) {
		horizontalAndVerticalBehavior = new HorizontalAndVerticalBehavior(board);
		diagonalBehavior = new DiagonalBehavior(board);
	}

	@Override
	public boolean verify(int x, int y) {
		return horizontalAndVerticalBehavior.verify(x,y) && diagonalBehavior.verify(x,y);
	}

	@Override
	public void increaseThreat(int x, int y) {
		horizontalAndVerticalBehavior.increaseThreat(x, y);
		diagonalBehavior.increaseThreat(x, y);
	}

	@Override
	public void decreaseThreat(int x, int y) {
		horizontalAndVerticalBehavior.decreaseThreat(x, y);
		diagonalBehavior.decreaseThreat(x, y);
	}

}
