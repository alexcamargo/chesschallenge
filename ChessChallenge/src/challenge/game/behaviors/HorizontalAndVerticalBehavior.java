package challenge.game.behaviors;

import challenge.game.Board;
import challenge.game.Board.Command;

public class HorizontalAndVerticalBehavior {

	Board board;
	
	public HorizontalAndVerticalBehavior(Board board){
		this.board = board;
	}

	public void decreaseThreat(int x, int y) {
		decreaseThreat(x, y, Integer.MAX_VALUE);
	}
	
	public void decreaseThreat(int x, int y, int maxDistance) {
		executeCommandHorizontalAndVertical(x, y, maxDistance, board.getDecrease());
	}

	public void increaseThreat(int x, int y) {
		increaseThreat(x, y, Integer.MAX_VALUE);
	}
	
	public void increaseThreat(int x, int y, int maxDistance) {
		executeCommandHorizontalAndVertical(x, y, maxDistance, board.getIncrease());
	}
	
	public boolean verify(int x, int y) {
		return verify(x, y, Integer.MAX_VALUE);
	}
	
	public boolean verify(int x, int y, int maxDistance) {
		
		boolean up = true, down = true, left = true, right = true;
		int i = 1;
		while(i <= maxDistance && (up || down || left || right)){
			if(up){
				if(x - i < 0)
					up= false;
				else if (board.getSlotValue(x - i, y) == -1) 
					return false;
			}
			
			if(down){
				if(x + i > board.getHeight() -1)
					down= false;
				else if (board.getSlotValue(x + i, y) == -1) 
					return false;
			}
			
			if(left){
				if(y - i < 0)
					left = false;
				else if (board.getSlotValue(x, y -i) == -1) 
					return false;
			}
			
			if(right){
				if(y + i > board.getWidth() -1)
					right= false;
				else if (board.getSlotValue(x, y +i) == -1) 
					return false;
			}
			i++;
		}
		
		return true;
	}

	private void executeCommandHorizontalAndVertical(int x, int y, int maxDistance, Command command) {
		boolean up = true, down = true, left = true, right = true;
		int i = 1;
		while(i <= maxDistance && (up || down || left || right)){
			if(up){
				if(x - i < 0)
					up= false;
				else
					command.execute(x - i, y);
			}
			
			if(down){
				if(x + i > board.getHeight() -1)
					down= false;
				else
					command.execute(x + i, y);
			}
			
			if(left){
				if(y - i < 0)
					left = false;
				else
					command.execute(x, y - i);
			}
			
			if(right){
				if(y + i > board.getWidth() -1)
					right= false;
				else
					command.execute(x, y + i);
			}	
			i++;
		}
	}

}
