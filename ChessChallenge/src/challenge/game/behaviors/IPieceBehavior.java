package challenge.game.behaviors;

public interface IPieceBehavior {
	
	boolean verify(int x, int y);
	void increaseThreat(int x, int y);
	void decreaseThreat(int x, int y);
}
