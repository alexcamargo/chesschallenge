package challenge.game.behaviors;

import challenge.game.Board;

public class RookBehavior implements IPieceBehavior {
	
	private HorizontalAndVerticalBehavior horizontalAndVerticalBehavior;
	
	public RookBehavior(Board board) {
		horizontalAndVerticalBehavior = new HorizontalAndVerticalBehavior(board);
	}

	@Override
	public boolean verify(int x, int y) {
		return horizontalAndVerticalBehavior.verify(x,y);
	}

	@Override
	public void increaseThreat(int x, int y) {
		horizontalAndVerticalBehavior.increaseThreat(x, y);
	}

	@Override
	public void decreaseThreat(int x, int y) {
		horizontalAndVerticalBehavior.decreaseThreat(x, y);
	}

}
