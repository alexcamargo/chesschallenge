package challenge.game.behaviors;

import challenge.game.Board;

public class KingBehavior implements IPieceBehavior {
	
	private HorizontalAndVerticalBehavior horizontalAndVerticalBehavior;
	private DiagonalBehavior diagonalBehavior;
	
	public KingBehavior(Board board) {
		horizontalAndVerticalBehavior = new HorizontalAndVerticalBehavior(board);
		diagonalBehavior = new DiagonalBehavior(board);
	}

	@Override
	public boolean verify(int x, int y) {
		return horizontalAndVerticalBehavior.verify(x, y, 1) && diagonalBehavior.verify(x, y, 1);
	}

	@Override
	public void increaseThreat(int x, int y) {
		horizontalAndVerticalBehavior.increaseThreat(x, y, 1);
		diagonalBehavior.increaseThreat(x, y, 1);

	}

	@Override
	public void decreaseThreat(int x, int y) {
		horizontalAndVerticalBehavior.decreaseThreat(x, y, 1);
		diagonalBehavior.decreaseThreat(x, y, 1);
	}

}
