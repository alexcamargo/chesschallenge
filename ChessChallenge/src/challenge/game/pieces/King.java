package challenge.game.pieces;

import challenge.game.Piece;
import challenge.game.PieceType;

public class King extends Piece {
	public King(){
		super(PieceType.King);
	}
	
	public King(int x, int y) {
		super(x,y, PieceType.King);
	}
}
