package challenge.game.pieces;

import challenge.game.Piece;
import challenge.game.PieceType;

public class Queen extends Piece {
	
	public Queen(){
		super(PieceType.Queen);
	}
	
	public Queen(int x, int y) {
		super(x,y, PieceType.Queen);
	}
}
