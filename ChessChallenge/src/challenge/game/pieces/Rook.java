package challenge.game.pieces;

import challenge.game.Piece;
import challenge.game.PieceType;

public class Rook extends Piece {
	
	public Rook(){
		super(PieceType.Rook);
	}
	
	public Rook(int x, int y) {
		super(x,y, PieceType.Rook);
	}
}
