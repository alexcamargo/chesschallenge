package challenge.game.pieces;

import challenge.game.Piece;
import challenge.game.PieceType;

public class Bishop extends Piece {

	public Bishop(){
		super(PieceType.Bishop);
	}
	
	public Bishop(int x, int y) {
		super(x,y, PieceType.Bishop);
	}
}
