package challenge.game.pieces;

import challenge.game.Piece;
import challenge.game.PieceType;

public class Knight extends Piece {
	public Knight(){
		super(PieceType.Knight);
	}
	
	public Knight(int x, int y) {
		super(x,y, PieceType.Knight);
	}
}
