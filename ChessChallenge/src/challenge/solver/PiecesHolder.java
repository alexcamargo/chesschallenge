package challenge.solver;

import challenge.game.PieceType;

public class PiecesHolder {
	private int rooksCount=0;
	private int bishopCount = 0;
	private int queenCount = 0;
	private int kingCount = 0;
	private int knightCount = 0;
	
	public PiecesHolder(ChallengeInput input){
		rooksCount = input.getRooks();
		bishopCount  = input.getBishops();
		queenCount  = input.getQueens();
		kingCount   = input.getKings();	
		knightCount   = input.getKnight();	
	}
	
	public PieceType getNextPiece(){
		
		if(queenCount > 0){
			queenCount--;
			return PieceType.Queen;
		}
		
		if(rooksCount > 0){
			rooksCount--;
			return PieceType.Rook;
		}
		
		if(bishopCount > 0){
			bishopCount --;
			return PieceType.Bishop;
		}
		
		if(kingCount > 0){
			kingCount--;
			return PieceType.King;
		}
		
		if(knightCount > 0){
			knightCount--;
			return PieceType.Knight;
		}
		return null;
	}

	public void returnPiece(PieceType pieceType) {
		switch(pieceType){
			case Rook:
				rooksCount++;
				break;
			case Bishop:
				bishopCount++;
				break;
			case Queen:
				queenCount++;
				break;
			case King:
				kingCount++;
				break;
			case Knight:
				knightCount++;
				break;
			default:
				return;
		}
	}

}
