package challenge.solver;

import challenge.game.Piece;
import challenge.game.PieceType;
import challenge.game.pieces.Bishop;
import challenge.game.pieces.King;
import challenge.game.pieces.Knight;
import challenge.game.pieces.Queen;
import challenge.game.pieces.Rook;

public class PieceFactory {

	public static Piece CreatePiece(PieceType pieceType, int x, int y) {
		switch(pieceType){
			case Rook: return new Rook(x, y);
			case Bishop: return new Bishop(x, y);
			case Queen: return new Queen(x, y);
			case King: return new King(x, y);
			case Knight: return new Knight(x, y);
			default: 
				throw new RuntimeException("Invalid piece type: "+pieceType);
		}
	}

}
