package challenge.solver;

public class ChallengeInput {

	private int rooks = 0;
	private int bishops = 0;
	private int queens = 0;
	private int kings = 0;
	private int knight = 0;
	private int m;
	private int n;
	
	public ChallengeInput(int m, int n){
		this.m = m;
		this.n = n;
	}
	
	public void setPieces(int kings, int queens, int knights, int bishops, int rooks){
		this.kings = kings;
		this.queens = queens;
		this.knight = knights;
		this.bishops = bishops;
		this.rooks = rooks;				
	}
	
	public int getM() {
		return m;
	}
	
	public int getN() {
		return n;
	}
	
	public void setRooks(int rooks) {
		this.rooks = rooks;
	}
	
	public int getRooks() {
		return rooks;
	}

	public void setBishop(int bishops) {
		this.bishops = bishops;
	}
	
	public int getBishops() {
		return bishops;
	}

	public void setQueen(int queens) {
		this.queens  = queens;
	}
	
	public int getQueens() {
		return queens;
	}

	public void setKings(int kings) {
		this.kings = kings;
	}
	
	public int getKings() {
		return kings;
	}

	public void setKnights(int knight) {
		this.knight = knight;
	}
	
	public int getKnight() {
		return knight;
	}	
}
