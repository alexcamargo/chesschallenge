package challenge.solver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Stack;

import challenge.game.Board;
import challenge.game.Piece;
import challenge.game.PieceType;

public class ChallengeSolver {

	public static Collection<ChallengeResult> Solve(ChallengeInput input) {
		List<ChallengeResult> results = new ArrayList<>();
		Board board = new Board(input.getM(), input.getN());
		PiecesHolder pieceHolder = new PiecesHolder(input);

		putNexPiece(board, results, new Stack<Piece>(), pieceHolder);

		return results;
	}

	private static void putNexPiece(Board board, List<ChallengeResult> results, Stack<Piece> currentPiecesStack,
			PiecesHolder pieceHolder) {
		PieceType pieceType = pieceHolder.getNextPiece();
		if (pieceType == null) {
			if(!currentPiecesStack.isEmpty())
				results.add(new ChallengeResult(currentPiecesStack));
			return;
		}

		int x = 0, y = 0;
		if (currentPiecesStack.size() > 0) {
			Piece lastPiece = currentPiecesStack.peek();
			if (lastPiece.getPieceType() == pieceType) {
				x = lastPiece.getX();
				y = lastPiece.getY();
			}
		}

		while (x < board.getHeight()){
			while(y < board.getWidth()) {
				if (board.canPutPieceInSlot(pieceType, x, y)) {
					Piece piece = PieceFactory.CreatePiece(pieceType, x, y);
					board.putPiece(piece);
					currentPiecesStack.push(piece);
					putNexPiece(board, results, currentPiecesStack, pieceHolder);
					board.removePiece(currentPiecesStack.pop());
				}
				y++;
			}
			y = 0;
			x++;
		}

		pieceHolder.returnPiece(pieceType);

	}
}
