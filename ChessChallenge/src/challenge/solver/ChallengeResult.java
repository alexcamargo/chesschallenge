package challenge.solver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import challenge.game.Piece;

public class ChallengeResult {

	private Collection<Piece> pieces;
	public ChallengeResult() {
		this.pieces = new ArrayList<>();
	}
	
	public ChallengeResult(List<Piece> pieces) {
		this.pieces = new ArrayList<>(pieces);
	}
	
	public void AddPiece(Piece piece) {
		pieces.add(piece);
	}
	
	public Collection<Piece> getPieces() {
		return pieces;
	}
	
	@Override
	public int hashCode() {
		int hash = 0;
		for(Piece p: this.pieces){
			hash += p.hashCode();
		}
		return hash;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof ChallengeResult))
			return false;
		ChallengeResult cr = (ChallengeResult)obj;
		
		return this.pieces.containsAll(cr.pieces);
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for(Piece piece : pieces){
			if(sb.length()==0)
				sb.append(piece);
			else
				sb.append(", "+piece);
		}
		
		return sb.toString();
	}
}
