package challenge;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;

import challenge.game.Piece;
import challenge.solver.ChallengeResult;

public class Display {
	
	private char[][] board;
	
	public Display(int m, int n){
		board = new char[m][];
		for(int i =0; i< m; i++){
			board[i] = new char[n]; 
		}
	}
	
	public void printResult(Writer writer, ChallengeResult result) throws IOException{
		clearBoard();
		putPieces(result.getPieces());
		printBoard(writer);
	}

	private void printBoard(Writer writer) throws IOException {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i< board.length;i++){
			sb.append(board[i][0]+" ");
			for(int j = 1; j< board[i].length;j++){
				sb.append(board[i][j]+" ");
			}
			sb.append(System.lineSeparator());
		}		
		writer.write(sb.toString());
	}

	private void putPieces(Collection<Piece> pieces) {
		for(Piece piece: pieces){
			board[piece.getX()][piece.getY()] = piece.getType();
		}		
	}

	private void clearBoard() {
		for(int i = 0; i< board.length;i++){
			for(int j = 0; j< board[i].length;j++){
				board[i][j] = '0';
			}
		}
	}
	

}
