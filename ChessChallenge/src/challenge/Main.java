package challenge;

import java.util.Scanner;

import challenge.solver.ChallengeInput;

public class Main {

	public static void main(String[] args) {
		processMain(args);		
	}

	private static void processMain(String[] args) {
		String outputFile = "output.txt";
		ChallengeInput input = null;
		
		if(args.length > 6){
			int m = Integer.parseInt(args[0]);
			int n = Integer.parseInt(args[1]);
			int bishops = Integer.parseInt(args[2]);
			int kings = Integer.parseInt(args[3]);
			int knights = Integer.parseInt(args[4]);
			int queens = Integer.parseInt(args[5]);
			int rooks = Integer.parseInt(args[6]);
			if(args.length == 8)
				outputFile = args[7];
			input = new ChallengeInput(m, n);
			input.setPieces(kings, queens, knights, bishops, rooks);
		} else if (args.length <= 1){
			if(args.length == 1){
				outputFile = args[0];
			}
			try{
				input = askForInput();			
			}catch(RuntimeException ex){
				if(ex.getMessage() == "QUIT"){
					System.out.println("Bye!");
				}else {
					ex.printStackTrace();
				}
				System.exit(0);	
			}
		} else {
			System.out.println("Invalid parameters");		
			System.out.println("Check README for the input parameters");
		}
		
		ExecuteSolver.execute(input, outputFile);
		
	}

	private static ChallengeInput askForInput() {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Welcome to Challenge Solver");
		System.out.println("Please, enter the following parameters. Type q to quit. Just enter for [default]");
		int m,n,bishops, kings, knights, queens, rooks;
		
		m = getInput(scanner, "Board height: ",false);
		n = getInput(scanner, "Board width: ",false);
		bishops = getInput(scanner, "Number of bishops[0]: ",true, 0);
		kings= getInput(scanner, "Number of kings[0]: ",true, 0);
		knights= getInput(scanner, "Number of knights[0]: ",true, 0);
		queens= getInput(scanner, "Number of queens[0]: ", true, 0);
		rooks= getInput(scanner, "Number of rooks[0]: ", true, 0);
		
		ChallengeInput input = new ChallengeInput(m, n);
		input.setPieces(kings, queens, knights, bishops, rooks);
		
		return input;
	}
	
	private static int getInput(Scanner scanner, String inputMessage, boolean allowedZero){
		return getInput(scanner, inputMessage, allowedZero, null);
	}
		
	private static int getInput(Scanner scanner, String inputMessage, boolean allowedZero, Integer defaultValue){
		boolean validInput = false;
		int returnVal;
		do{
			returnVal = -1; 
			System.out.print(inputMessage);
			String input = scanner.nextLine();
			if(input.trim().equals("q"))
				throw new RuntimeException("QUIT");
			
			if(input.trim().isEmpty() && defaultValue != null)
				return defaultValue;
				
			try{
				returnVal = Integer.parseInt(input);
			} catch(NumberFormatException exc){
			}
			
			if(returnVal>0 || (allowedZero && returnVal == 0))
				validInput = true;
			else
				System.out.println("Invalid input. Please enter and positive integer"+(allowedZero? ", zero ":"")+" or 'q' to quit.");
			
		}while(!validInput);
		return returnVal;
	}
}
