package challenge;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.time.Duration;
import java.time.Instant;
import java.util.Collection;

import challenge.solver.ChallengeInput;
import challenge.solver.ChallengeResult;
import challenge.solver.ChallengeSolver;

public class ExecuteSolver {

	public static void execute(ChallengeInput input, String outputFileName) {
		boolean usingStandardOutput = false;
		Writer writer = null;
		try {
			writer = new FileWriter(outputFileName);
		} catch (IOException e) {
			System.out.println("Couldn't open the file. Using standard output.");
			writer = new PrintWriter(System.out);
			usingStandardOutput = true;
		}

		System.out.println("Processing...");

		Instant beginning = Instant.now();
		Collection<ChallengeResult> results = ChallengeSolver.Solve(input);
		Instant afterProcess = Instant.now();
		Duration duration = Duration.between(beginning, afterProcess);

		System.out.println("Process time: " + duration.getSeconds() + " seconds");		
		if (results.isEmpty())
			System.out.println("There is no result for the input");
		else
			System.out.println("Results found: " + results.size());
		
		System.out.println("-----------------------------");
		if (!results.isEmpty()) {

			try {
				if (writer != null) {
					System.out.println("Writing output to file " + outputFileName);
					int i = 1;
					Display display = new Display(input.getM(), input.getN());
					for (ChallengeResult result : results) {
						writer.write("Result " + i + System.lineSeparator());
						display.printResult(writer, result);
						writer.write("-----------------------------" + System.lineSeparator());

						i++;
					}
					Instant fileProcess = Instant.now();
					duration = Duration.between(afterProcess, fileProcess);

					System.out.println("Time to write to file: " + duration.getSeconds() + " seconds");
					System.out.println("-----------------------------");
				}
			} catch (IOException e) {
				System.out.println("Couldn't write the file");
				e.printStackTrace();
			}
		}

		try {
			if (!usingStandardOutput && writer != null) {
				writer.flush();
				writer.close();
			}
		} catch (IOException e) {
			System.out.println("Couldn't close the file");
			e.printStackTrace();
		}

		Instant end = Instant.now();
		duration = Duration.between(beginning, end);

		System.out.println("Full process time: " + duration.getSeconds() + " seconds");
		System.out.println("-----------------------------");
	}
}
