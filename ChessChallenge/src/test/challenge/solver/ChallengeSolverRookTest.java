package test.challenge.solver;

import java.util.Collection;

import org.testng.Assert;
import org.testng.annotations.Test;

import challenge.solver.ChallengeInput;
import challenge.solver.ChallengeResult;
import challenge.solver.ChallengeSolver;

public class ChallengeSolverRookTest {

	@Test
	public void SolveWith1x1BoardAnd1Rook(){
		ChallengeInput input = new ChallengeInput(1,1);
		input.setRooks(1);
		
		Collection<ChallengeResult> results = ChallengeSolver.Solve(input);
		
		Assert.assertEquals(results.size(), 1);
		Assert.assertTrue(ChallengeSolverTestResult.getBoard1x1And1Rook().containsAll(results));
	}
	
	@Test
	public void SolveWith2x2BoardAnd1Rook(){
		ChallengeInput input = new ChallengeInput(2,2);
		input.setRooks(1);
		
		Collection<ChallengeResult> results = ChallengeSolver.Solve(input);
		
		Assert.assertEquals(results.size(), 4);
		Assert.assertTrue(ChallengeSolverTestResult.getBoard2x2And1Rook().containsAll(results));
	}
	
	@Test
	public void SolveWith2x2BoardAnd2Rooks(){
		ChallengeInput input = new ChallengeInput(2,2);
		input.setRooks(2);
		
		Collection<ChallengeResult> results = ChallengeSolver.Solve(input);
		
		Assert.assertEquals(results.size(), 2);
		Assert.assertTrue(ChallengeSolverTestResult.getBoard2x2And2Rook().containsAll(results));
	}
	
	@Test
	public void SolveWith3x3BoardAnd2Rooks(){
		ChallengeInput input = new ChallengeInput(3,3);
		input.setRooks(2);
		
		Collection<ChallengeResult> results = ChallengeSolver.Solve(input);
		
		Assert.assertEquals(results.size(), 18);
		Assert.assertTrue(ChallengeSolverTestResult.getBoard3x3And2Rook().containsAll(results));
	}
}
