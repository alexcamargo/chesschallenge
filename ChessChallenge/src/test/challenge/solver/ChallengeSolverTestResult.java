package test.challenge.solver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import challenge.game.Piece;
import challenge.game.pieces.Bishop;
import challenge.game.pieces.King;
import challenge.game.pieces.Knight;
import challenge.game.pieces.Queen;
import challenge.game.pieces.Rook;
import challenge.solver.ChallengeResult;

public class ChallengeSolverTestResult {

	private static ChallengeResult createResult(Piece... pieces) {
		ChallengeResult cr = new ChallengeResult();

		for (Piece p : pieces) {
			cr.AddPiece(p);
		}
		return cr;
	}

	public static void printResultList(Collection<ChallengeResult> results) {
		System.out.println("Number of results: " + results.size());
		for (ChallengeResult cr : results) {
			System.out.println(cr.toString());
		}
		System.out.println();
	}

	public static Collection<ChallengeResult> getBoard1x1And1Rook() {
		List<ChallengeResult> results = new ArrayList<>();
		ChallengeResult cr = new ChallengeResult();
		cr.AddPiece(new Rook(0, 0));
		results.add(cr);

		return results;
	}

	public static Collection<ChallengeResult> getBoard2x2And1Rook() {
		List<ChallengeResult> results = new ArrayList<ChallengeResult>();
		results.add(createResult(new Rook(0, 0)));
		results.add(createResult(new Rook(0, 1)));
		results.add(createResult(new Rook(1, 0)));
		results.add(createResult(new Rook(1, 1)));
		return results;
	}

	public static Collection<ChallengeResult> getBoard2x2And2Rook() {
		List<ChallengeResult> results = new ArrayList<ChallengeResult>();
		results.add(createResult(new Rook(0, 0), new Rook(1, 1)));
		results.add(createResult(new Rook(1, 0), new Rook(0, 1)));
		return results;
	}

	public static Collection<ChallengeResult> getBoard3x3And2Rook() {
		List<ChallengeResult> results = new ArrayList<ChallengeResult>();

		results.add(createResult(new Rook(0, 0), new Rook(1, 1)));
		results.add(createResult(new Rook(0, 0), new Rook(1, 2)));
		results.add(createResult(new Rook(0, 0), new Rook(2, 1)));
		results.add(createResult(new Rook(0, 0), new Rook(2, 2)));

		results.add(createResult(new Rook(0, 1), new Rook(1, 0)));
		results.add(createResult(new Rook(0, 1), new Rook(1, 2)));
		results.add(createResult(new Rook(0, 1), new Rook(2, 0)));
		results.add(createResult(new Rook(0, 1), new Rook(2, 2)));

		results.add(createResult(new Rook(0, 2), new Rook(1, 0)));
		results.add(createResult(new Rook(0, 2), new Rook(1, 1)));
		results.add(createResult(new Rook(0, 2), new Rook(2, 0)));
		results.add(createResult(new Rook(0, 2), new Rook(2, 1)));

		results.add(createResult(new Rook(1, 0), new Rook(2, 1)));
		results.add(createResult(new Rook(1, 0), new Rook(2, 2)));

		results.add(createResult(new Rook(1, 1), new Rook(2, 0)));
		results.add(createResult(new Rook(1, 1), new Rook(2, 2)));

		results.add(createResult(new Rook(1, 2), new Rook(2, 0)));
		results.add(createResult(new Rook(1, 2), new Rook(2, 1)));

		return results;
	}

	public static Collection<ChallengeResult> getBoard1x1And1Bishop() {
		List<ChallengeResult> results = new ArrayList<ChallengeResult>();
		results.add(createResult(new Bishop(0, 0)));
		return results;
	}
	
	public static Collection<ChallengeResult> getBoard2x1And1Bishop() {
		List<ChallengeResult> results = new ArrayList<ChallengeResult>();
		results.add(createResult(new Bishop(0, 0), new Bishop(1, 0)));
		return results;
	}

	public static Collection<ChallengeResult> getBoard2x2And1Bishop() {
		List<ChallengeResult> results = new ArrayList<ChallengeResult>();
		results.add(createResult(new Bishop(0, 0)));
		results.add(createResult(new Bishop(1, 0)));
		results.add(createResult(new Bishop(0, 1)));
		results.add(createResult(new Bishop(1, 1)));
		return results;
	}

	public static Collection<ChallengeResult> getBoard2x2And2Bishop() {
		List<ChallengeResult> results = new ArrayList<ChallengeResult>();
		results.add(createResult(new Bishop(0, 0), new Bishop(1, 0)));
		results.add(createResult(new Bishop(0, 0), new Bishop(0, 1)));
		results.add(createResult(new Bishop(1, 1), new Bishop(0, 1)));
		results.add(createResult(new Bishop(1, 1), new Bishop(1, 0)));
		return results;
	}

	public static Collection<ChallengeResult> getBoard3x3And3Bishop() {
		List<ChallengeResult> results = new ArrayList<ChallengeResult>();
		results.add(createResult(new Bishop(0, 0), new Bishop(0, 1), new Bishop(0, 2)));
		results.add(createResult(new Bishop(0, 0), new Bishop(0, 1), new Bishop(2, 0)));
		results.add(createResult(new Bishop(0, 0), new Bishop(0, 1), new Bishop(2, 1)));
		results.add(createResult(new Bishop(0, 0), new Bishop(0, 2), new Bishop(1, 0)));
		results.add(createResult(new Bishop(0, 0), new Bishop(0, 2), new Bishop(1, 2)));
		results.add(createResult(new Bishop(0, 0), new Bishop(0, 2), new Bishop(2, 1)));
		results.add(createResult(new Bishop(0, 0), new Bishop(1, 0), new Bishop(1, 2)));
		results.add(createResult(new Bishop(0, 0), new Bishop(1, 0), new Bishop(2, 0)));
		results.add(createResult(new Bishop(0, 0), new Bishop(1, 2), new Bishop(2, 0)));
		results.add(createResult(new Bishop(0, 0), new Bishop(2, 0), new Bishop(2, 1)));
		results.add(createResult(new Bishop(0, 1), new Bishop(0, 2), new Bishop(2, 1)));
		results.add(createResult(new Bishop(0, 1), new Bishop(0, 2), new Bishop(2, 2)));
		results.add(createResult(new Bishop(0, 1), new Bishop(1, 1), new Bishop(2, 1)));
		results.add(createResult(new Bishop(0, 1), new Bishop(2, 0), new Bishop(2, 1)));
		results.add(createResult(new Bishop(0, 1), new Bishop(2, 0), new Bishop(2, 2)));
		results.add(createResult(new Bishop(0, 1), new Bishop(2, 1), new Bishop(2, 2)));
		results.add(createResult(new Bishop(0, 2), new Bishop(1, 0), new Bishop(1, 2)));
		results.add(createResult(new Bishop(0, 2), new Bishop(1, 0), new Bishop(2, 2)));
		results.add(createResult(new Bishop(0, 2), new Bishop(1, 2), new Bishop(2, 2)));
		results.add(createResult(new Bishop(0, 2), new Bishop(2, 1), new Bishop(2, 2)));
		results.add(createResult(new Bishop(1, 0), new Bishop(1, 1), new Bishop(1, 2)));
		results.add(createResult(new Bishop(1, 0), new Bishop(1, 2), new Bishop(2, 0)));
		results.add(createResult(new Bishop(1, 0), new Bishop(1, 2), new Bishop(2, 2)));
		results.add(createResult(new Bishop(1, 0), new Bishop(2, 0), new Bishop(2, 2)));
		results.add(createResult(new Bishop(2, 0), new Bishop(1, 2), new Bishop(2, 2)));
		results.add(createResult(new Bishop(2, 0), new Bishop(2, 1), new Bishop(2, 2)));

		return results;
	}

	public static Collection<ChallengeResult> getBoard1x1And1Queen() {
		List<ChallengeResult> results = new ArrayList<>();
		ChallengeResult cr = new ChallengeResult();
		cr.AddPiece(new Queen(0, 0));
		results.add(cr);

		return results;
	}

	public static Collection<ChallengeResult> getBoard2x2And1Queen() {
		List<ChallengeResult> results = new ArrayList<ChallengeResult>();
		results.add(createResult(new Queen(0, 0)));
		results.add(createResult(new Queen(1, 0)));
		results.add(createResult(new Queen(0, 1)));
		results.add(createResult(new Queen(1, 1)));
		return results;
	}

	public static Collection<ChallengeResult> getBoard3x3And2Queen() {
		List<ChallengeResult> results = new ArrayList<ChallengeResult>();
		results.add(createResult(new Queen(0, 0), new Queen(1, 2)));
		results.add(createResult(new Queen(0, 0), new Queen(2, 1)));
		results.add(createResult(new Queen(0, 1), new Queen(2, 0)));
		results.add(createResult(new Queen(0, 1), new Queen(2, 2)));
		results.add(createResult(new Queen(0, 2), new Queen(1, 0)));
		results.add(createResult(new Queen(0, 2), new Queen(2, 1)));
		results.add(createResult(new Queen(1, 0), new Queen(2, 2)));
		results.add(createResult(new Queen(1, 2), new Queen(2, 0)));

		return results;
	}

	public static Collection<ChallengeResult> getBoard1x1And1King() {
		List<ChallengeResult> results = new ArrayList<>();
		ChallengeResult cr = new ChallengeResult();
		cr.AddPiece(new King(0, 0));
		results.add(cr);

		return results;
	}

	public static Collection<ChallengeResult> getBoard2x2And1King() {
		List<ChallengeResult> results = new ArrayList<ChallengeResult>();
		results.add(createResult(new King(0, 0)));
		results.add(createResult(new King(0, 1)));
		results.add(createResult(new King(1, 0)));
		results.add(createResult(new King(1, 1)));
		return results;
	}

	public static Collection<ChallengeResult> getBoard3x3And2King() {
		List<ChallengeResult> results = new ArrayList<ChallengeResult>();
		results.add(createResult(new King(0, 0), new King(0, 2)));
		results.add(createResult(new King(0, 0), new King(1, 2)));
		results.add(createResult(new King(0, 0), new King(2, 0)));
		results.add(createResult(new King(0, 0), new King(2, 1)));
		results.add(createResult(new King(0, 0), new King(2, 2)));
		results.add(createResult(new King(0, 1), new King(2, 0)));
		results.add(createResult(new King(0, 1), new King(2, 1)));
		results.add(createResult(new King(0, 1), new King(2, 2)));
		results.add(createResult(new King(0, 2), new King(1, 0)));
		results.add(createResult(new King(0, 2), new King(2, 0)));
		results.add(createResult(new King(0, 2), new King(2, 1)));
		results.add(createResult(new King(0, 2), new King(2, 2)));
		results.add(createResult(new King(1, 0), new King(1, 2)));
		results.add(createResult(new King(1, 0), new King(2, 2)));
		results.add(createResult(new King(1, 2), new King(2, 0)));
		results.add(createResult(new King(2, 0), new King(2, 2)));
		
		return results;
	}
	
	public static Collection<ChallengeResult> getBoard1x1And1Knight() {
		List<ChallengeResult> results = new ArrayList<ChallengeResult>();
		results.add(createResult(new Knight(0, 0)));
		return results;
	}
	
	public static Collection<ChallengeResult> getBoard2x2And1Knight() {
		List<ChallengeResult> results = new ArrayList<ChallengeResult>();
		results.add(createResult(new Knight(0, 0)));
		results.add(createResult(new Knight(0, 1)));
		results.add(createResult(new Knight(1, 0)));
		results.add(createResult(new Knight(1, 1)));

		return results;
	}
	
	public static Collection<ChallengeResult> getBoard2x2And2Knight() {
		List<ChallengeResult> results = new ArrayList<ChallengeResult>();
		results.add(createResult(new Knight(0, 0), new Knight(0, 1)));
		results.add(createResult(new Knight(0, 0), new Knight(1, 0)));
		results.add(createResult(new Knight(0, 0), new Knight(1, 1)));
		results.add(createResult(new Knight(0, 1), new Knight(1, 0)));
		results.add(createResult(new Knight(0, 1), new Knight(1, 1)));
		results.add(createResult(new Knight(1, 0), new Knight(1, 1)));
		
		return results;
	}
	

	public static Collection<ChallengeResult> getBoard3x3And2Knight() {
		List<ChallengeResult> results = new ArrayList<ChallengeResult>();
		results.add(createResult(new Knight(0, 0), new Knight(0, 1)));
		results.add(createResult(new Knight(0, 0), new Knight(0, 2)));
		results.add(createResult(new Knight(0, 0), new Knight(1, 0)));
		results.add(createResult(new Knight(0, 0), new Knight(1, 1)));
		results.add(createResult(new Knight(0, 0), new Knight(2, 0)));
		results.add(createResult(new Knight(0, 0), new Knight(2, 2)));
		results.add(createResult(new Knight(0, 1), new Knight(0, 2)));
		results.add(createResult(new Knight(0, 1), new Knight(1, 0)));
		results.add(createResult(new Knight(0, 1), new Knight(1, 1)));
		results.add(createResult(new Knight(0, 1), new Knight(1, 2)));
		results.add(createResult(new Knight(0, 1), new Knight(2, 1)));
		results.add(createResult(new Knight(0, 2), new Knight(1, 1)));
		results.add(createResult(new Knight(0, 2), new Knight(1, 2)));
		results.add(createResult(new Knight(0, 2), new Knight(2, 0)));
		results.add(createResult(new Knight(0, 2), new Knight(2, 2)));
		results.add(createResult(new Knight(1, 0), new Knight(1, 1)));
		results.add(createResult(new Knight(1, 0), new Knight(1, 2)));
		results.add(createResult(new Knight(1, 0), new Knight(2, 0)));
		results.add(createResult(new Knight(1, 0), new Knight(2, 1)));
		results.add(createResult(new Knight(1, 1), new Knight(1, 2)));
		results.add(createResult(new Knight(1, 1), new Knight(2, 0)));
		results.add(createResult(new Knight(1, 1), new Knight(2, 1)));
		results.add(createResult(new Knight(1, 1), new Knight(2, 2)));
		results.add(createResult(new Knight(1, 2), new Knight(2, 1)));
		results.add(createResult(new Knight(1, 2), new Knight(2, 2)));
		results.add(createResult(new Knight(2, 0), new Knight(2, 1)));
		results.add(createResult(new Knight(2, 0), new Knight(2, 2)));
		results.add(createResult(new Knight(2, 1), new Knight(2, 2)));
		
		return results;
	}

}
