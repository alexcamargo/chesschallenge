package test.challenge.solver;

import java.util.Collection;

import org.testng.Assert;
import org.testng.annotations.Test;

import challenge.solver.ChallengeInput;
import challenge.solver.ChallengeResult;
import challenge.solver.ChallengeSolver;

public class ChallengeSolverKingTest {

	@Test
	public void SolveWith1x1BoardAnd1King(){
		ChallengeInput input = new ChallengeInput(1,1);
		input.setKings(1);
		
		Collection<ChallengeResult> results = ChallengeSolver.Solve(input);
		
		Assert.assertEquals(results.size(), 1);
		Assert.assertTrue(ChallengeSolverTestResult.getBoard1x1And1King().containsAll(results));
	}
	
	@Test
	public void SolveWith2x2BoardAnd1King(){
		ChallengeInput input = new ChallengeInput(2,2);
		input.setKings(1);
		
		Collection<ChallengeResult> results = ChallengeSolver.Solve(input);
		
		Assert.assertEquals(results.size(), 4);
		Assert.assertTrue(ChallengeSolverTestResult.getBoard2x2And1King().containsAll(results));
	}
	
	@Test
	public void SolveWith3x3BoardAnd2Kings(){
		ChallengeInput input = new ChallengeInput(3,3);
		input.setKings(2);
		
		Collection<ChallengeResult> results = ChallengeSolver.Solve(input);
		
		Collection<ChallengeResult> expectedResults = ChallengeSolverTestResult.getBoard3x3And2King();
		Assert.assertEquals(results.size(), expectedResults.size());
		Assert.assertTrue(expectedResults.containsAll(results));
	}
}
