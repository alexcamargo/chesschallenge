package test.challenge.solver;

import java.util.Collection;

import org.testng.Assert;
import org.testng.annotations.Test;

import challenge.solver.ChallengeInput;
import challenge.solver.ChallengeResult;
import challenge.solver.ChallengeSolver;

public class ChallengeSolverQueenTest {
	
	@Test
	public void SolveWith1x1BoardAnd1Queen(){
		ChallengeInput input = new ChallengeInput(1,1);
		input.setQueen(1);
		
		Collection<ChallengeResult> results = ChallengeSolver.Solve(input);
		
		Collection<ChallengeResult> expectedResults = ChallengeSolverTestResult.getBoard1x1And1Queen();
		Assert.assertEquals(results.size(), expectedResults.size());
		Assert.assertTrue(expectedResults.containsAll(results));
	}
	
	@Test
	public void SolveWith2x2BoardAnd1Queen(){
		ChallengeInput input = new ChallengeInput(2,2);
		input.setQueen(1);
		
		Collection<ChallengeResult> results = ChallengeSolver.Solve(input);
		
		Collection<ChallengeResult> expectedResults = ChallengeSolverTestResult.getBoard2x2And1Queen();
		Assert.assertEquals(results.size(), expectedResults.size());
		Assert.assertTrue(expectedResults.containsAll(results));
	}
	
	@Test
	public void SolveWith3x3BoardAnd2Queen(){
		ChallengeInput input = new ChallengeInput(3,3);
		input.setQueen(2);
		
		Collection<ChallengeResult> results = ChallengeSolver.Solve(input);
		
		Collection<ChallengeResult> expectedResults = ChallengeSolverTestResult.getBoard3x3And2Queen();
		Assert.assertEquals(results.size(), expectedResults.size());
		Assert.assertTrue(expectedResults.containsAll(results));
	}
		
	@Test
	public void SolveWith8x8BoardAnd8Queen(){
		ChallengeInput input = new ChallengeInput(8,8);
		input.setQueen(8);
		
		Collection<ChallengeResult> results = ChallengeSolver.Solve(input);
		
		// There are 92 distinct solution for the eight queen problem
		Assert.assertEquals(results.size(), 92);
	}
	
	

}
