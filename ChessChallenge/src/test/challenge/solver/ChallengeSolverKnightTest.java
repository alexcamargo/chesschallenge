package test.challenge.solver;

import java.util.Collection;

import org.testng.Assert;
import org.testng.annotations.Test;

import challenge.solver.ChallengeInput;
import challenge.solver.ChallengeResult;
import challenge.solver.ChallengeSolver;

public class ChallengeSolverKnightTest {

	@Test
	public void SolveWith1x1BoardAnd1Knight(){
		ChallengeInput input = new ChallengeInput(1,1);
		input.setKnights(1);
		
		Collection<ChallengeResult> results = ChallengeSolver.Solve(input);
		
		Assert.assertEquals(results.size(), 1);
		Assert.assertTrue(ChallengeSolverTestResult.getBoard1x1And1Knight().containsAll(results));
	}
	
	@Test
	public void SolveWith2x2BoardAnd1Knight(){
		ChallengeInput input = new ChallengeInput(2,2);
		input.setKnights(1);
		
		Collection<ChallengeResult> results = ChallengeSolver.Solve(input);
		
		Assert.assertEquals(results.size(), 4);
		Assert.assertTrue(ChallengeSolverTestResult.getBoard2x2And1Knight().containsAll(results));
	}
	
	@Test
	public void SolveWith3x3BoardAnd2Knights(){
		ChallengeInput input = new ChallengeInput(3,3);
		input.setKnights(2);
		
		Collection<ChallengeResult> results = ChallengeSolver.Solve(input);
		
		Collection<ChallengeResult> expectedResults = ChallengeSolverTestResult.getBoard3x3And2Knight();
		Assert.assertEquals(results.size(), expectedResults.size());
		Assert.assertTrue(expectedResults.containsAll(results));
	}
	
	@Test
	public void SolveWith3x3BoardAnd4Knights(){
		ChallengeInput input = new ChallengeInput(3,3);
		input.setKnights(4);
		
		Collection<ChallengeResult> results = ChallengeSolver.Solve(input);
		
		Assert.assertEquals(results.size(), 18);
		
	}
}
