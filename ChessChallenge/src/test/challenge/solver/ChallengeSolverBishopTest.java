package test.challenge.solver;

import java.util.Collection;

import org.testng.Assert;
import org.testng.annotations.Test;

import challenge.solver.ChallengeInput;
import challenge.solver.ChallengeResult;
import challenge.solver.ChallengeSolver;

public class ChallengeSolverBishopTest {
	
	@Test
	public void SolveWith1x1BoardAnd1Bishop(){
		ChallengeInput input = new ChallengeInput(1,1);
		input.setBishop(1);
		
		Collection<ChallengeResult> results = ChallengeSolver.Solve(input);
		
		Collection<ChallengeResult> expectedResults = ChallengeSolverTestResult.getBoard1x1And1Bishop();
		Assert.assertEquals(results.size(), expectedResults.size());
		Assert.assertTrue(expectedResults.containsAll(results));
	}
	
	@Test
	public void SolveWith2x1BoardAnd2Bishop(){
		ChallengeInput input = new ChallengeInput(2,1);
		input.setBishop(2);
		
		Collection<ChallengeResult> results = ChallengeSolver.Solve(input);
		
		Collection<ChallengeResult> expectedResults = ChallengeSolverTestResult.getBoard2x1And1Bishop();
		Assert.assertEquals(results.size(), expectedResults.size());
		Assert.assertTrue(expectedResults.containsAll(results));
	}

	@Test
	public void SolveWith2x2BoardAnd1Bishop(){
		ChallengeInput input = new ChallengeInput(2,2);
		input.setBishop(1);
		
		Collection<ChallengeResult> results = ChallengeSolver.Solve(input);
		
		Collection<ChallengeResult> expectedResults = ChallengeSolverTestResult.getBoard2x2And1Bishop();
		ChallengeSolverTestResult.printResultList(results);
		ChallengeSolverTestResult.printResultList(expectedResults);
		Assert.assertEquals(results.size(), expectedResults.size());
		Assert.assertTrue(expectedResults.containsAll(results));
	}
	
	@Test
	public void SolveWith2x2BoardAnd2Bishop(){
		ChallengeInput input = new ChallengeInput(2,2);
		input.setBishop(2);
		
		Collection<ChallengeResult> results = ChallengeSolver.Solve(input);
		
		Collection<ChallengeResult> expectedResults = ChallengeSolverTestResult.getBoard2x2And2Bishop();
//		ChallengeSolverTestResult.printResultList(results);
//		ChallengeSolverTestResult.printResultList(expectedResults);
		Assert.assertEquals(results.size(), expectedResults.size());
		Assert.assertTrue(expectedResults.containsAll(results));
	}
	
	@Test
	public void SolveWith3x3BoardAnd3Bishop(){
		ChallengeInput input = new ChallengeInput(3,3);
		input.setBishop(3);
		
		Collection<ChallengeResult> results = ChallengeSolver.Solve(input);
		
		Collection<ChallengeResult> expectedResults = ChallengeSolverTestResult.getBoard3x3And3Bishop();
		ChallengeSolverTestResult.printResultList(results);
		ChallengeSolverTestResult.printResultList(expectedResults);
		Assert.assertEquals(results.size(), expectedResults.size());
		Assert.assertTrue(expectedResults.containsAll(results));
	}
}
