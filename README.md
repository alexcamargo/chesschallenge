# README #

### What is this repository for? ###

This repo hosts the Chess Challenge Solver Project.

The Chess Challenge Solver, solves the problem of putting N chess pieces on the board without they threatening each other. 

### How do I get set up? ###

* Have java installed
* Have maven (I used eclipse maven project for this)
* Have TestNG runner (I used the eclipse plugin for running the tests)
    - Link to the plugin http://testng.org/doc/download.html

### Who do I talk to? ###
Talk to me Alex Camargo by email alexccamargo@gmail.com